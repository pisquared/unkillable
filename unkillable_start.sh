#!/bin/bash
mkdir -p /sys/fs/cgroup/freezer
if ! mount | grep -q freezer; then
  mount -t cgroup -ofreezer freezer /sys/fs/cgroup/freezer
fi
mkdir -p /sys/fs/cgroup/freezer/frozen

while true; do 
  if ps aux | grep -q "[u]nkillable_real.sh"; then
    echo THAWED > /sys/fs/cgroup/freezer/frozen/freezer.state
    sleep 0.1
    echo FROZEN > /sys/fs/cgroup/freezer/frozen/freezer.state
    sleep 1
  else
    ./unkillable_real.sh & disown
    pid=`ps aux | grep "[u]nkillable_real.sh" | awk '{print $2}'`
    echo $pid > /sys/fs/cgroup/freezer/frozen/tasks
    echo FROZEN > /sys/fs/cgroup/freezer/frozen/freezer.state
  fi
done
