# (Almost) unkillable process

This creates a service that starts a process in a loop, puts it in uninterruptable sleep (so that we can't kill with `kill -9`). The stop of the service is also starting a process but it returns quickly so that systemctl doesn't time out.

The current way to diffuse is:
```
sudo systemctl stop unkillable
sudo sh -c "echo THAWED > /sys/fs/cgroup/freezer/frozen/freezer.state"
sudo killall unkillable_real.sh
```
